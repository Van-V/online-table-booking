import React from 'react';

class OpenClose extends React.Component{
	render(){
		var d = new Date()
		var hr = d.getHours()
		var min = d.getMinutes()
		var notify

		if((hr > 10 && hr < 14) || (hr == 10 && min >= 30)) {
				notify = "Open"
			 }else if((hr > 17 && hr < 22) || (hr == 17 && min >= 1)) {
				notify = "Open"
			 }else {
				notify = "Closed"
			}	

		return(
			<div>
				We are <span className="notify">{notify}</span>
			</div>
		);
	}
}
export default OpenClose;