import React from 'react';
import {render} from 'react-dom';
import RestaurantOpen from './RestaurantOpen.jsx';
import OpenClose from './OpenClose.jsx';
import AllTables from './AllTables.jsx';

class App extends React.Component {
	constructor() {
		super();
		this.state = {
			timings: []
		}
	}
	componentWillMount(){
		this.setState({ timings: [{
			id: 1,
			time: '5.00pm' 
		},{
			id: 2,
			time: '5.30pm'
		},{
			id: 3,
			time: '6.00pm'
		},{
			id: 4,
			time: '6.30pm'
		}]})
		}
	render(){
		return (
			<div>
				<OpenClose />
				<AllTables timings={this.state.timings} />
			</div>
		);
	}
}
render (<App />, document.getElementById('app'));

/*<RestaurantOpen /><br/> */