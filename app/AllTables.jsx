import React from 'react';

class AllTables extends React.Component{
	constructor(){
		super();
		this.state = {
			timings: ''
		}
		this.bookaTable = this.bookaTable.bind(this);
	}
	bookaTable() {
		console.log("from book a table");
		console.log(this.props.timings);
	}
	render() {

		
		return (
			<div>
			<br/>
				<button className="btn btn-success" onClick={this.bookaTable}>Table for 2</button>
				<button className="btn btn-success">Table for 3</button>
			</div>
		);
	}
}
export default AllTables;