import React from 'react';
import {render} from 'react-dom';

class Restaurant extends React.Component {
	render(){
		return(
			<div>
				<h1>{this.props.name}</h1>
			</div>
		);
	}
}
//export default Restaurant;
React.render(<Restaurant name="Indian Summer" />, document.getElementById('openTiming'));