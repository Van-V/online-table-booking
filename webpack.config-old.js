// var webpack = require('webpack');

// module.exports = {
// 	entry: './script.jsx',
// 	output: {filename: 'appbundle.js'},

// 	module: {
// 		loaders: [
// 			{test: /\.jsx?/, loader: 'babel-loader', query: { presets:['react'] }, exclude: /node_modules/}
// 		]
// 	}

// }

var webpack = require('webpack');
//var path = require('path');

//var BUILD_DIR = path.resolve(__dirname, 'src/client/public');
///var APP_DIR = path.resolve(__dirname, 'src/client/app');

var config = {
  entry: './app/index.jsx',
  output: {
    filename: 'appbundle.js'
  },

  module : {
    loaders : [
      {
        test : /\.jsx?/,
        exclude: /node_modules/,
        loader : 'babel',
        query  :{
                presets:['react','es2015']
            }
      }
    ]
  }
};

module.exports = config;
