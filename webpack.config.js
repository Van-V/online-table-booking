var webpack = require('webpack');
var path = require('path');

var config = {
  entry: './app/index.jsx',
  output: {
    filename: 'appbundle.js'
  },
  devServer: {
      inline: true,
      port: 3000
   },

  module : {
    loaders : [
      {
        test : /\.jsx?/,
        exclude: /node_modules/,
        loader : 'babel',
        query  :{
                presets:['react','es2015']
            }
      }
    ]
  }
};

module.exports = config;
